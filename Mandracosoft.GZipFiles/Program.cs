﻿using System;
using System.IO;
using CommandLine;
using System.Collections.Generic;
using ICSharpCode.SharpZipLib.GZip;

namespace Mandracosoft.GZipFiles;

internal class Program
{

    private static Options _options;

    private static void Main(string[] args)
    {

        Parser.Default.ParseArguments<Options>(args)
            .WithParsed(RunOptions)
            .WithNotParsed(HandleParseError);

        if (_options is null)
            return;

        var directoryInfo = new DirectoryInfo(_options.Path);

        Console.WriteLine(directoryInfo.FullName);

        if (!directoryInfo.Exists)
            return;

        var fileSystemInfos = directoryInfo
            .GetFileSystemInfos(_options.SearchPattern);

        foreach (var fileSystemInfo in fileSystemInfos)
            SingleFile(fileSystemInfo);

        Console.WriteLine("Press any key to continue");

        Console.ReadKey();

    }

    private static void RunOptions(Options options) => _options = options;

    private static void HandleParseError(IEnumerable<Error> errors) { }

    private static void SingleFile(FileSystemInfo fileSystemInfo)
    {

        if (!fileSystemInfo.Exists)
            return;

        Console.WriteLine(fileSystemInfo.Name);

        SingleGZip(fileSystemInfo);

    }

    private static void SingleGZip(FileSystemInfo fileSystemInfo)
    {

        try
        {

            var target = new FileInfo(fileSystemInfo.FullName + ".gz");

            if (!_options.Override && target.Exists)
                return;

            var targetFileStream = File.Create(target.FullName);

            using Stream stream = new GZipOutputStream(targetFileStream);

            using var sourceFileStream = File.OpenRead(fileSystemInfo.FullName);

            var buffer = new byte[sourceFileStream.Length];

            var count = (int)sourceFileStream.Length;

            var _ = sourceFileStream.Read(buffer, 0, count);

            stream.Write(buffer, 0, buffer.Length);

            Console.WriteLine(target.Name);

        }
        catch (Exception e)
        {

            Console.WriteLine(e);

        }

    }

}