﻿using CommandLine;

namespace Mandracosoft.GZipFiles
{

    internal class Options
    {

        [Option('o', "Override", Required = false, HelpText = "", Default = false)]
        public bool Override { get; set; }

        [Option('s', "SearchPattern", Required = true, HelpText = "")]
        public string SearchPattern { get; set; }

        [Option('p', "Path", Required = false, HelpText = "", Default = ".")]
        public string Path { get; set; }

    }

}
