var target = Argument("target", "Pack");

var configuration = Argument("configuration", "Release");

var solutionDirectory = "./";

var projectDirectory = $"{solutionDirectory}/Mandracosoft.GZipFiles";

var dotNetBuildSettings = new DotNetBuildSettings
{
    NoLogo = true,
    NoRestore = true,
    Configuration = configuration
};

var dotNetPublishSettings = new DotNetPublishSettings
{
    NoLogo = true,
    NoRestore = true,
    Configuration = configuration,
    NoBuild = true,
    OutputDirectory = "./publish"
};

var dotNetPackSettings = new DotNetPackSettings
{
    NoLogo = true,
    NoRestore = true,
    Configuration = configuration,
    NoBuild = true,
    OutputDirectory = "./publish"
};

var deleteDirectorySettings = new DeleteDirectorySettings 
{ 
    Recursive = true, 
    Force = true 
};

Task("Clean")
    .Does(() => 
    {
        var directoriesToDelete = GetDirectories("./**/Release")
        .Concat(GetDirectories("./publish"));
        DeleteDirectories(directoriesToDelete, deleteDirectorySettings);
    });

Task("Clean.Full")
    .ContinueOnError()
    .IsDependentOn("Clean")
    .Does(() => 
    {
        var directoriesToDelete = GetDirectories("./**/obj")
        .Concat(GetDirectories("./**/bin"))
        .Concat(GetDirectories("./**/publish"));
        DeleteDirectories(directoriesToDelete, deleteDirectorySettings);
    });

Task("Restore")
    .Does(() => DotNetRestore(solutionDirectory));

Task("Build")
    .IsDependentOn("Clean")
    .IsDependentOn("Restore")
    .Does(() => DotNetBuild(solutionDirectory, dotNetBuildSettings));

Task("Publish")
    .IsDependentOn("Build")
    .Does(() => DotNetPublish(projectDirectory, dotNetPublishSettings));

Task("Pack")
    .IsDependentOn("Publish")
    .Does(() => DotNetPack(projectDirectory, dotNetPackSettings));

Task("Install")
    .Does(() => 
    {
        var processSettings = new ProcessSettings
        {
            Arguments = "tool install --global --add-source ./publish Mandracosoft.GZipFiles",
        };
        StartProcess("dotnet", processSettings);
    });

Task("Uninstall")
    .Does(() => 
    {
        var processSettings = new ProcessSettings
        {
            Arguments = "tool uninstall --global Mandracosoft.GZipFiles",
        };
        StartProcess("dotnet", processSettings);
    });

RunTarget(target);